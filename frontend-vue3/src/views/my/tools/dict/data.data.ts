//import { getAllRoleList, isAccountExist } from '/@/api/demo/system';
import { BasicColumn } from '/@/components/Table';
import { FormSchema } from '/@/components/Table';
import { getdicttypeconststype, getrolelist, getorgtree, getposlist } from '../../system-api';
import { Input, Tag } from 'ant-design-vue';
import { defineComponent, h } from 'vue';
//import { values } from 'lodash-es';
export const columns: BasicColumn[] = [
  {
    title: '类型名称',
    dataIndex: 'name',
    width: 120,
  },
  {
    title: '唯一编码',
    dataIndex: 'code',
    width: 120,
  },
  {
    title: '排序',
    dataIndex: 'sort',
    width: 120,
  },
  {
    title: '状态',
    dataIndex: 'status',
    width: 120,
    customRender: ({ record }) => {
      const status = record.status;
      const enable = status === 0;
      const color = enable ? 'green' : 'red';
      const text = enable ? '启动' : '禁用';
      return h(Tag, { color: color }, () => text);
    },
  },
  {
    title: '备注',
    dataIndex: 'remark',
  },
];

export const searchFormSchema: FormSchema[] = [
  {
    field: 'name',
    label: '名称',
    component: 'Input',
    colProps: { span: 8 },
  },
  {
    field: 'code',
    label: '编码',
    component: 'Input',
    colProps: { span: 8 },
  },
];

export const accountFormSchema: FormSchema[] = [
  {
    field: 'id',
    label: 'id',
    component: 'Input',
    required: true,
    ifShow: true,
    dynamicDisabled: true,
  },
  {
    label: '类型名称',
    field: 'name',
    component: 'Input',
    required: true,
  },
  {
    label: '唯一编码',
    field: 'code',
    component: 'Input',
    required: true,
  },
  {
    field: 'status',
    label: '状态',
    component: 'RadioButtonGroup',
    defaultValue: 0,
    componentProps: {
      options: [
        { label: '启动', value: 0 },
        { label: '禁用', value: 1 },
      ],
    },
    required: true,
  },
  {
    field: 'sort',
    label: '排序',
    component: 'Input',
    required: true,
  },

  {
    label: '备注',
    field: 'remark',
    component: 'InputTextArea',
    required: false,
  },
];
