//import { getAllRoleList, isAccountExist } from '/@/api/demo/system';
import { BasicColumn } from '/@/components/Table';
import { FormSchema } from '/@/components/Table';
import {
  getRequestTypeEnum,
  getrolelist,
  getTypeEnumbyfid,
  getTypeEnum,
  timerapi,
} from '../../system-api';
import { Input, Switch } from 'ant-design-vue';
import { defineComponent, h } from 'vue';
import { setRoleStatus } from '/@/api/demo/system';
import { useMessage } from '/@/hooks/web/useMessage';
//import { values } from 'lodash-es';
export const columns: BasicColumn[] = [
  {
    title: '任务名称',
    dataIndex: 'jobName',
  },
  {
    title: '请求地址',
    dataIndex: 'requestUrl',
  },
  {
    title: '请求类型',
    dataIndex: 'requestType',
  },
  {
    title: '请求参数',
    dataIndex: 'requestParameters',
  },
  {
    title: '间隔',
    dataIndex: 'interval',
  },
  {
    title: 'cron',
    dataIndex: 'cron',
  },
  {
    title: '状态',
    dataIndex: 'timerStatus',
    customRender: ({ record }) => {
      if (!Reflect.has(record, 'pendingStatus')) {
        record.pendingStatus = false;
      }
      return h(Switch, {
        checked: record.timerStatus === 0,
        checkedChildren: '已启用',
        unCheckedChildren: '已停止',
        loading: record.pendingStatus,
        async onChange(checked: boolean) {
          record.pendingStatus = false;
          const newStatus = checked ? 0 : 1;
          const { createMessage } = useMessage();
          let ret;
          console.log(record, record.timerStatus);
          if (record.timerStatus == 1) {
            ret = (await timerapi.sysTimersStartPost(record)).data.code;
            if (ret == 204) {
              record.timerStatus = newStatus;
              createMessage.success(`已经开启任务`);
            } else {
              createMessage.error(`开启失败`);
            }
          } else {
            ret = (await timerapi.sysTimersStopPost(record)).data.code;
            if (ret == 204) {
              record.timerStatus = newStatus;
              createMessage.success(`已经停止任务`);
            } else {
              createMessage.error(`停止失败`);
            }
          }

        },
      });
    },
  },
  {
    title: '执行次数',
    dataIndex: 'runNumber',
  },
  {
    title: '备注',
    dataIndex: 'remark',
  },
];

export const searchFormSchema: FormSchema[] = [
  {
    field: 'name',
    label: '名称',
    component: 'Input',
    colProps: { span: 6 },
  },
  {
    field: 'code',
    label: '编码',
    component: 'Input',
    colProps: { span: 6 },
  },
  {
    field: 'groupcode',
    label: '所属分类',
    component: 'ApiSelect',
    componentProps: {
      labelField: 'value',
      valueField: 'code',
    },
    colProps: { span: 6 },
  },
];

export const accountFormSchema: FormSchema[] = [
  {
    field: 'id',
    label: 'id',
    component: 'Input',
    required: true,
    ifShow: true,
    dynamicDisabled: true,
  },
  {
    label: '任务名称',
    field: 'jobName',
    component: 'Input',
    required: true,
  },
  {
    label: '请求地址',
    field: 'requestUrl',
    component: 'Input',
    required: true,
  },
  {
    field: 'requestType',
    label: '请求类型',
    component: 'ApiSelect',
    componentProps: {
      api: getTypeEnum,
      params: 'RequestTypeEnum',
      labelField: 'value',
      valueField: 'code',
    },
    required: true,
  },
  {
    label: '配置项参数',
    field: 'requestParameters',
    component: 'Input',
  },
  {
    label: '请求头',
    field: 'headers',
    component: 'Input',
  },
  {
    field: 'executeType',
    label: '定时器类型',
    component: 'ApiSelect',
    componentProps: {
      api: getTypeEnumbyfid,
      params: { ename: 'SysTimer', fname: 'TimerType' },
      labelField: 'value',
      valueField: 'code',
    },
    required: true,
  },
  {
    field: 'interval',
    label: '执行间隔(秒)',
    component: 'Input',
  },
  {
    field: 'cron',
    label: 'cron',
    component: 'Input',
  },
  {
    field: 'startNow',
    label: '立即执行',
    component: 'RadioButtonGroup',
    defaultValue: false,
    componentProps: {
      options: [
        { label: '是', value: true },
        { label: '否', value: false },
      ],
    },
  },
  {
    field: 'doOnce',
    label: '立即执行',
    component: 'RadioButtonGroup',
    defaultValue: false,
    componentProps: {
      options: [
        { label: '是', value: true },
        { label: '否', value: false },
      ],
    },
  },
  {
    field: 'timerStatus',
    label: '执行类型',
    component: 'ApiSelect',
    componentProps: {
      api: getTypeEnumbyfid,
      params: { ename: 'SysTimer', fname: 'ExecuteType' },
      labelField: 'value',
      valueField: 'code',
    },
    required: true,
  },
  {
    label: '备注',
    field: 'remark',
    component: 'InputTextArea',
    required: false,
  },
];
