import { BasicColumn } from '/@/components/Table';
import { FormSchema } from '/@/components/Table';
import { h } from 'vue';
import { Switch, Tag } from 'ant-design-vue';
import { setRoleStatus } from '/@/api/demo/system';
import { useMessage } from '/@/hooks/web/useMessage';

export const columns: BasicColumn[] = [
  {
    title: '访问人',
    dataIndex: 'name',
    width: 100,
  },
  {
    title: '访问账号',
    dataIndex: 'account',
    width: 100,
  },
  {
    title: '访问类型',
    dataIndex: 'visType',
    width: 100,
    customRender: ({ record }) => {
      const status = record.visType;
      let text;
      switch (status) {
        case 0:
          text = '登陆';
          break;
        case 1:
          text = '登出';
          break;
        case 2:
          text = '注册';
          break;
        case 3:
          text = '改密';
          break;
        case 4:
          text = '授权登陆';
          break;
      }
      return text;
    },
  },
  {
    title: '是否成功',
    dataIndex: 'success',
    width: 80,
    customRender: ({ record }) => {
      const status = record.success;
      const enable = status === 0;
      const color = enable ? 'green' : 'red';
      const text = enable ? '是' : '否';
      return h(Tag, { color: color }, () => text);
    },
  },
  {
    title: 'ip',
    dataIndex: 'ip',
    width: 180,
  },
  {
    title: '浏览器',
    dataIndex: 'browser',
    width: 100,
  },
  {
    title: '访问时间',
    dataIndex: 'visTime',
    width: 100,
  },
];

export const searchFormSchema: FormSchema[] = [
  {
    field: 'name',
    label: '访问人名称',
    component: 'Input',
    colProps: { span: 8 },
  },
  {
    field: 'visType',
    label: '访问类型',
    component: 'Select',
    componentProps: {
      options: [
        { label: '登陆', value: 0 },
        { label: '登出', value: 1 },
        { label: '注册', value: 2 },
        { label: '改密', value: 3 },
        { label: '授权登陆', value: 4 },
      ],
    },

    colProps: { span: 8 },
  },
  {
    field: 'success',
    label: '是否成功',
    component: 'Select',
    componentProps: {
      options: [
        { label: '是', value: 0 },
        { label: '否', value: 1 },
      ],
    },
    colProps: { span: 8 },
  },
  {
    field: 'searchValue',
    label: '关键字',
    component: 'Input',
    colProps: { span: 8 },
  },
  {
    field: 'visTime',
    label: '操作时间',
    component: 'RangePicker',
    colProps: { span: 8 },
  },
];

export const formSchema: FormSchema[] = [
  {
    field: 'account',
    label: '操作账号',
    component: 'Input',
  },
  {
    field: 'name',
    label: '操作人',
    component: 'Input',
  },
  {
    field: 'browser',
    label: '浏览器',
    component: 'Input',
  },
  {
    field: 'ip',
    label: 'ip',
    component: 'Input',
  },
  {
    field: 'visType',
    label: '访问类型',
    component: 'Select',
    componentProps: {
      options: [
        { label: '登陆', value: 0 },
        { label: '登出', value: 1 },
        { label: '注册', value: 2 },
        { label: '改密', value: 3 },
        { label: '授权登陆', value: 4 },
      ],
    },

    colProps: { span: 8 },
  },
  {
    field: 'success',
    label: '是否成功',
    component: 'Select',
    componentProps: {
      options: [
        { label: '是', value: 0 },
        { label: '否', value: 1 },
      ],
    },
    colProps: { span: 8 },
  },
  {
    field: 'visTime',
    label: '时间',
    component: 'Input',
  },
  {
    field: 'message',
    label: '信息',
    component: 'InputTextArea',
  },
];
