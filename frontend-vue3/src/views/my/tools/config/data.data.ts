//import { getAllRoleList, isAccountExist } from '/@/api/demo/system';
import { BasicColumn } from '/@/components/Table';
import { FormSchema } from '/@/components/Table';
import { getdicttypeconststype, getrolelist, getorgtree, getposlist } from '../../system-api';
import { Input } from 'ant-design-vue';
import { defineComponent, h } from 'vue';
//import { values } from 'lodash-es';
export const columns: BasicColumn[] = [
  {
    title: '参数名',
    dataIndex: 'name',
    width: 120,
  },
  {
    title: '唯一编码',
    dataIndex: 'code',
    width: 120,
  },
  {
    title: '参数值',
    dataIndex: 'value',
    width: 180,
  },
  {
    title: '所属分类',
    dataIndex: 'groupCode',
    width: 180,
  },
  {
    title: '备注',
    dataIndex: 'remark',
  },
];

export const searchFormSchema: FormSchema[] = [
  {
    field: 'name',
    label: '名称',
    component: 'Input',
    colProps: { span: 6},
  },
  {
    field: 'code',
    label: '编码',
    component: 'Input',
    colProps: { span: 6 },
  },
  {
    field: 'groupcode',
    label: '所属分类',
    component: 'ApiSelect',
    componentProps: {
      api: getdicttypeconststype,
      labelField: 'value',
      valueField: 'code',
    },
    colProps: { span: 6 },
  },
];

export const accountFormSchema: FormSchema[] = [
  {
    field: 'id',
    label: 'id',
    component: 'Input',
    required: true,
    ifShow: true,
    dynamicDisabled: true,
  },
  {
    label: '参数名称',
    field: 'name',
    component: 'Input',
    required: true,
  },
  {
    label: '唯一编码',
    field: 'code',
    component: 'Input',
    required: true,
    dynamicDisabled: true,
  },
  {
    field: 'sysFlag',
    label: '系统参数',
    component: 'RadioButtonGroup',
    defaultValue: 1,
    componentProps: {
      options: [
        { label: '是', value: 'Y' },
        { label: '否', value: 'N' },
      ],
    },
    required: true,
    dynamicDisabled: true,
  },
  {
    field: 'groupCode',
    label: '所属分类',
    component: 'ApiSelect',
    //defaultValue: [142307070910548, 142307070910547],
    componentProps: {
      api: getdicttypeconststype,
      labelField: 'value',
      valueField: 'code',
    },
    required: true,
    dynamicDisabled: true,
  },
  {
    field: 'value',
    label: '参数值',
    component: 'Input',
    required: true,
  },

  {
    label: '备注',
    field: 'remark',
    component: 'InputTextArea',
    required: false,
  },
];
