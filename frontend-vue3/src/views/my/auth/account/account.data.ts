//import { getAllRoleList, isAccountExist } from '/@/api/demo/system';
import { BasicColumn } from '/@/components/Table';
import { FormSchema } from '/@/components/Table';
import { getdicttypelist, getrolelist, getorgtree, getposlist } from '../../system-api';
import { Input } from 'ant-design-vue';
import { defineComponent, h } from 'vue';
//import { values } from 'lodash-es';

export const columns: BasicColumn[] = [
  {
    title: '用户名',
    dataIndex: 'account',
    width: 120,
  },
  {
    title: '昵称',
    dataIndex: 'nickName',
    width: 120,
  },
  {
    title: '姓名',
    dataIndex: 'name',
    width: 120,
  },
  {
    title: '创建时间',
    dataIndex: 'birthday',
    width: 180,
  },
  {
    title: '邮箱',
    dataIndex: 'email',
    width: 200,
  },
  {
    title: '备注',
    dataIndex: 'remark',
  },
];

export const searchFormSchema: FormSchema[] = [
  {
    field: 'searchvalue',
    label: '搜索关键字',
    component: 'Input',
    colProps: { span: 8 },
  },
  {
    field: 'searchstatus',
    label: '状态',
    component: 'ApiSelect',
    componentProps: {
      api: getdicttypelist,
      labelField: 'value',
      valueField: 'code',
    },
    colProps: { span: 8 },
  },
];

export const accountFormSchema: FormSchema[] = [
  {
    field: 'id',
    label: 'id',
    component: 'Input',
    required: true,
    ifShow: true,
    dynamicDisabled: true,
  },
  {
    field: 'account',
    label: '用户名',
    component: 'Input',
    helpMessage: ['本字段演示异步验证', '不能输入带有admin的用户名'],
    rules: [
      {
        required: true,
        message: '请输入用户名',
      },
    ],
  },
  {
    field: 'password',
    label: '密码',
    component: 'InputPassword',
    required: true,
    ifShow: true,
  },
  {
    field: 'confirm',
    label: '确认密码',
    component: 'InputPassword',
    required: true,
    ifShow: true,
  },
  {
    label: '姓名',
    field: 'name',
    component: 'Input',
    required: true,
  },
  {
    field: 'roleId',
    label: '角色信息',
    component: 'ApiSelect',
    //defaultValue: [142307070910548, 142307070910547],
    componentProps: {
      mode: 'multiple',
      api: getrolelist,
      labelField: 'name',
      valueField: 'id',
    },
    required: false,
  },
  {
    label: '昵称',
    field: 'nickName',
    component: 'Input',
    required: false,
  },
  {
    field: 'sex',
    label: '性别',
    component: 'RadioButtonGroup',
    defaultValue: 1,
    componentProps: {
      options: [
        { label: '男', value: 1 },
        { label: '女', value: 2 },
      ],
    },
    required: true,
  },
  {
    field: 'orgId',
    label: '所属部门',
    component: 'ApiTreeSelect',
    //defaultValue: 'sysEmpInfo.orgName',
    componentProps: {
      api: getorgtree,
      replaceFields: {
        title: 'title',
        key: 'title',
        value: 'value',
      },
      getPopupContainer: () => document.body,
      onChange: (e: any) => {
        //console.log(e);
      },
    },
    required: true,
  },
  {
    field: 'posId',
    label: '职位信息',
    component: 'ApiSelect',
    //defaultValue: [142307070910548, 142307070910547],
    componentProps: {
      mode: 'multiple',
      api: getposlist,
      labelField: 'name',
      valueField: 'id',
    },
    required: true,

  },
  {
    label: '工号',
    field: 'jobNum',
    component: 'Input',
    required: false,
  },
  {
    field: 'nickName',
    label: '昵称',
    component: 'Input',
    required: true,
  },

  {
    label: '邮箱',
    field: 'email',
    component: 'Input',
    required: false,
  },
  {
    label: '手机号',
    field: 'phone',
    component: 'Input',
    required: false,
  },
  {
    label: '备注',
    field: 'remark',
    component: 'InputTextArea',
  },
];