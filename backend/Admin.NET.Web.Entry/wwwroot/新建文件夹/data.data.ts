//import { getAllRoleList, isAccountExist } from '/@/api/demo/system';
import { BasicColumn } from '/@@/components/Table';
import { FormSchema } from '/@@/components/Table';
import { Input } from 'ant-design-vue';
import { defineComponent, h } from 'vue';
//import { values } from 'lodash-es';
export const columns: BasicColumn[] = [
  @foreach (var column in Model.TableField){
    if(@column.WhetherTable == "Y"){
      @:{
        @:title: '@column.ColumnComment',
        @:dataIndex: '@column.LowerColumnName',
        @:width: 100,
        @:},
    }

  }
];

export const searchFormSchema: FormSchema[] = [
  @foreach (var column in Model.QueryWhetherList){
    
      @:{
        @:label: '@column.ColumnComment',
        @:field: '@column.LowerColumnName',
        @:component: 'Input',
        @:colProps: { span: 7},
        @:},
    
  }
];

export const accountFormSchema: FormSchema[] = [

  {
    field: 'id',
    label: 'id',
    component: 'Input',
    required: true,
    ifShow: true,
    dynamicDisabled: true,
  },
  @foreach (var column in Model.TableField){
      @:{
        @:label: '@column.ColumnComment',
        @:field: '@column.LowerColumnName',
        @:width: 100,
        @:component: 'Input',
        @:required: true,
        @:},

  }
];
