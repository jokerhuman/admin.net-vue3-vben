﻿using Furion.Extras.Admin.NET;
using Furion.DatabaseAccessor;
using Furion.DatabaseAccessor.Extensions;
using Furion.DependencyInjection;
using Furion.DynamicApiController;
using Mapster;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Linq.Dynamic.Core;

namespace aaa
{
    /// <summary>
    /// aa服务
    /// </summary>
    [ApiDescriptionSettings("自己的业务", Name = "Car", Order = 100)]
    public class CarService : ICarService, IDynamicApiController, ITransient
    {
        private readonly IRepository<Car> _rep;

        public CarService(
            IRepository<Car> rep
        )
        {
            _rep = rep;
        }

        /// <summary>
        /// 分页查询aa
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        [HttpGet("/Car/page")]
        public async Task<dynamic> Page([FromQuery] CarInput input)
        {
            var entities = await _rep.DetachedEntities
                                     .Where(!string.IsNullOrEmpty(input.CarName), u => u.CarName == input.CarName)
                                     .Where(!string.IsNullOrEmpty(input.CarNo), u => u.CarNo == input.CarNo)
                                     .OrderBy(PageInputOrder.OrderBuilder<CarInput>(input))
                                     .ToPagedListAsync(input.PageNo, input.PageSize);
            var result = XnPageResult<Car>.PageResult<CarDto>(entities);
            await DtoMapper(result.Rows);
            return result;
        }

        /// <summary>
        /// 增加aa
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        [HttpPost("/Car/add")]
        public async Task Add(AddCarInput input)
        {
            var entity = input.Adapt<Car>();
            await entity.InsertAsync();
        }

        /// <summary>
        /// 删除aa
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        [HttpPost("/Car/delete")]
        public async Task Delete(DeleteCarInput input)
        {
            var entity = await _rep.FirstOrDefaultAsync(u => u.Id == input.Id);
            await entity.DeleteAsync();
        }

        /// <summary>
        /// 更新aa
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        [HttpPost("/Car/edit")]
        public async Task Update(UpdateCarInput input)
        {
            var entity = input.Adapt<Car>();
            await entity.UpdateAsync(true);
        }

        /// <summary>
        /// 获取aa
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        [HttpGet("/Car/detail")]
        public async Task<Car> Get([FromQuery] QueryeCarInput input)
        {
            return await _rep.DetachedEntities.FirstOrDefaultAsync(u => u.Id == input.Id);
        }

        /// <summary>
        /// 获取aa列表
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        [HttpGet("/Car/list")]
        public async Task<dynamic> List([FromQuery] CarInput input)
        {
            return await _rep.DetachedEntities.ToListAsync();
        }    

        private async Task DtoMapper(ICollection<CarDto> rows)
        {
            foreach (var item in rows)
            {
            }
        }
    }
}
