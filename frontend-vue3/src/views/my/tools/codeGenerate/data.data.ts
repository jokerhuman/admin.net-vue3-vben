//import { getAllRoleList, isAccountExist } from '/@/api/demo/system';
import { BasicColumn } from '/@/components/Table';
import { FormSchema } from '/@/components/Table';
import { getcodegentableByPage, getrolelist, getorgtree, getposlist, getMenuTree, getappList } from '../../system-api';
import { Input, Tag } from 'ant-design-vue';
import { defineComponent, h } from 'vue';
//import { values } from 'lodash-es';
export const columns: BasicColumn[] = [
  {
    title: '表名称',
    dataIndex: 'tableName',
    width: 100,
  },
  {
    title: '业务名',
    dataIndex: 'busName',
    width: 100,
  },
  {
    title: '命名空间',
    dataIndex: 'nameSpace',
    width: 100,
  },
  {
    title: '作者姓名',
    dataIndex: 'authorName',
    width: 100,
  },
  {
    title: '作者姓名',
    dataIndex: 'authorName',
    width: 100,
  },
  {
    title: '生成方式',
    dataIndex: 'generateType',
    width: 100,
  },
];

export const searchFormSchema: FormSchema[] = [
  {
    field: 'name',
    label: '名称',
    component: 'Input',
    colProps: { span: 8 },
  },
  {
    field: 'code',
    label: '编码',
    component: 'Input',
    colProps: { span: 8 },
  },
];

export const accountFormSchema: FormSchema[] = [
  {
    field: 'id',
    label: 'id',
    component: 'Input',
    required: true,
    ifShow: true,
    dynamicDisabled: true,
  },
  {
    label: '生成表',
    field: 'tableName',
    component: 'ApiSelect',
    componentProps: {
      api: getcodegentableByPage,
      labelField: 'tableName',
      valueField: 'tableName',
    },
    required: true,
  },
  {
    label: '业务名',
    field: 'busName',
    component: 'Input',
    required: true,
  },
  {
    field: 'menuApplication',
    label: '所属应用',
    component: 'ApiSelect',
    //defaultValue: [142307070910548, 142307070910547],
    componentProps: {
      api: getappList,
      labelField: 'name',
      valueField: 'code',
    },
    required: true,
  },
  {
    field: 'menuPid',
    label: '父级菜单',
    component: 'ApiTreeSelect',
    componentProps: {
      api: getMenuTree,
      replaceFields: {
        title: 'title',
        key: 'id',
        value: 'value',
      },
      getPopupContainer: () => document.body,
    },
    required: false,
  },
  {
    label: '命名空间',
    field: 'nameSpace',
    component: 'Input',
    required: false,
  },
  {
    label: '作者名称',
    field: 'authorName',
    component: 'Input',
    required: false,
  },
  {
    field: 'generateType',
    label: '生成方式',
    component: 'RadioButtonGroup',
    defaultValue: '2',
    componentProps: {
      options: [
        { label: '压缩包', value: '1' },
        { label: '本项目', value: '2' },
      ],
    },
    required: true,
  },
];
