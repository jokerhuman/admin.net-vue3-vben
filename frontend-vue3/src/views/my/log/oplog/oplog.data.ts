import { BasicColumn } from '/@/components/Table';
import { FormSchema } from '/@/components/Table';
import { h } from 'vue';
import { Switch } from 'ant-design-vue';
import { setRoleStatus } from '/@/api/demo/system';
import { useMessage } from '/@/hooks/web/useMessage';

export const columns: BasicColumn[] = [
  {
    title: '请求方式',
    dataIndex: 'reqMethod',
    width: 100,
  },
  {
    title: 'ip',
    dataIndex: 'ip',
    width: 100,
  },
  {
    title: '请求地址',
    dataIndex: 'url',
    width: 100,
  },
  {
    title: '执行结果',
    dataIndex: 'success',
    width: 100,
  },
  {
    title: '执行时间',
    dataIndex: 'opTime',
    width: 180,
  },
  {
    title: '执行人',
    dataIndex: 'name',
    width: 100,
  },
  {
    title: '执行账号',
    dataIndex: 'account',
    width: 100,
  },
];

export const searchFormSchema: FormSchema[] = [
  {
    field: 'name',
    label: '操作人',
    component: 'Input',
    colProps: { span: 8 },
  },
  {
    field: 'location',
    label: '请求地址',
    component: 'Input',
    colProps: { span: 8 },
  },
  {
    field: 'success',
    label: '是否成功',
    component: 'Select',
    componentProps: {
      options: [
        { label: '是', value: 0 },
        { label: '否', value: 1 },
      ],
    },
    colProps: { span: 8 },
  },
  {
    field: 'opTime',
    label: '操作时间',
    component: 'RangePicker',
    colProps: { span: 8 },
  },
];

export const formSchema: FormSchema[] = [
  {
    field: 'account',
    label: '执行人账号',
    component: 'Input',
  },
  {
    field: 'name',
    label: '用户名',
    component: 'Input',
  },
  {
    field: 'browser',
    label: '浏览器',
    component: 'Input',
  },
  {
    field: 'os',
    label: '操作系统',
    component: 'Input',
  },
  {
    field: 'className',
    label: '类名',
    component: 'Input',
  },

  {
    field: 'methodName',
    label: '方法名称',
    component: 'Input',
  },

  {
    field: 'location',
    label: '请求地址',
    component: 'Input',
  },
  {
    field: 'url',
    label: 'url',
    component: 'Input',
  },
  {
    field: 'ip',
    label: 'ip',
    component: 'Input',
  },
  {
    field: 'reqMethod',
    label: '请求类型',
    component: 'Input',
  },
  {
    field: 'param',
    label: '发送请求',
    component: 'InputTextArea',
  },
  {
    field: 'result',
    label: '返回结果',
    component: 'InputTextArea',
  },
  {
    field: 'opTime',
    label: '操作时间',
    component: 'Input',
  },
  {
    field: 'success',
    label: '是否成功',
    component: 'RadioButtonGroup',
    defaultValue: 0,
    componentProps: {
      options: [
        { label: '是', value: 0 },
        { label: '否', value: 1 },
      ],
    },
  },
];
