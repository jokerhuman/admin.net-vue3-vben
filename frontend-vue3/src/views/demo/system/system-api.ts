import { UserApi, OrgApi, DicttypeApi, RoleApi, PosApi, MenuApi, AppApi } from '/@/api_base/api';
import { AuthApi } from '/@/api_base';
export const authApi = new AuthApi(undefined, undefined, defHttp.getAxios());
import { defHttp } from '/@/utils/http/axios';
export const userApi = new UserApi(undefined, undefined, defHttp.getAxios());
export const getuserlist = async (params?: any) => {
  const userInfo = await userApi.sysUserPageGet(
    undefined,
    undefined,
    undefined,
    undefined,
    undefined,
    undefined,
    undefined,
    undefined,
    undefined,
    undefined,
    undefined,
    undefined,
    undefined,
    params.OrgId,
    undefined,
    undefined,
    undefined,
    params.searchstatus,
    undefined,
    undefined,
    undefined,
    params.searchvalue,
    params.page,
    params.pageSize
  );
  // console.log(userInfo);
  return userInfo.data.data.rows;
};
let orgtree;
export const orgApi = new OrgApi(undefined, undefined, defHttp.getAxios());
export const getorgListByPage = async (params?: any) => {
  const userInfo = await orgApi.sysOrgPageGet(
    1,
    undefined,
    undefined,
    params.searchvalue,
    undefined,
    params.page,
    params.pageSize,
    undefined,
    undefined,
    undefined,
    undefined
  );
  return userInfo.data.data.rows;
};
export const getorgtree = async () => {
  if (orgtree != null) {
    return orgtree;
  }
  const Info = await orgApi.sysOrgTreeGet();
  orgtree = Info.data.data;
  //console.log(Info.data);
  return Info.data.data;
};

export function forofa(intput: any, arr: any) {
  for (const info of arr) {
    if (info.value == intput) {
      return info.title;
    }
  }
}
export async function getorgname(intput: any) {
  let data;
  if (orgtree != null) {
    data = orgtree;
  } else {
    data = (await orgApi.sysOrgTreeGet()).data.data;
  }

  for (const info of data) {
    if (info.value == intput) {
      //console.log(info, intput, info.title);
      return info.title;
    }
    const ret = forofa(intput, info.children);

    if (ret != '') {
      return ret;
    }
  }
}
export const dicttypeApi = new DicttypeApi(undefined, undefined, defHttp.getAxios());
export const getdicttypelist = async () => {
  const Info = await dicttypeApi.sysDictTypeDropDownGet('common_status');
  //console.log(Info.data);
  return Info.data.data;
};

export const roleApi = new RoleApi(undefined, undefined, defHttp.getAxios());
export const getrolelist = async () => {
  const Info = await roleApi.sysRoleDropDownGet();
  return Info.data.data;
};
export const getRoleListByPage = async (params?: any) => {
  const userInfo = await roleApi.sysRolePageGet(
    1,
    params.searchvalue,
    undefined,
    undefined,
    params.page,
    params.pageSize,
    undefined,
    undefined,
    undefined,
    undefined
    //params.searchvalue
  );
  //console.log(userInfo);
  return userInfo.data.data.rows;
};

let poslist;
export const posApi = new PosApi(undefined, undefined, defHttp.getAxios());
export const getposListByPage = async (params?: any) => {
  const userInfo = await posApi.sysPosPageGet(
    params.searchvalue,
    undefined,
    undefined,
    undefined,
    undefined,
    params.page,
    params.pageSize
    //params.searchvalue
  );
  //console.log(userInfo);
  return userInfo.data.data.rows;
};
export const getposlist = async () => {
  if (poslist != null) {
    return poslist;
  }
  const Info = await posApi.sysPosListGet();
  poslist = Info.data.data;
  return Info.data.data;
};

export const menuApi = new MenuApi(undefined, undefined, defHttp.getAxios());
export const getMenuList = async (params?: any) => {
  const userInfo = await menuApi.sysMenuListGet(params.searchvalue, undefined);
  //console.log(userInfo);
  return userInfo.data.data;
};
export const getMenuTree = async () => {
  const userInfo = await menuApi.sysMenuTreeGet();
  //console.log(userInfo);
  return userInfo.data.data;
};

export const appApi = new AppApi(undefined, undefined, defHttp.getAxios());
export const getappList = async () => {
  const userInfo = await appApi.sysAppListGet();
  return userInfo.data.data;
};
