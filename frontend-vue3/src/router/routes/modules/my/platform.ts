import type { AppRouteModule } from '/@/router/types';

import { LAYOUT } from '/@/router/constant';
import { t } from '/@/hooks/web/useI18n';

const system: AppRouteModule = {
  path: '/platform',
  name: 'Platform',
  component: LAYOUT,
  redirect: '/system/menu',
  meta: {
    orderNo: 2000,
    icon: 'ion:settings-outline',
    title: '平台管理',
  },
  children: [
    {
      path: 'menu',
      name: 'MenuManagement',
      meta: {
        title: t('routes.demo.system.menu'),
        ignoreKeepAlive: true,
      },
      component: () => import('/@/views/my/platform/menu/index.vue'),
    },
    {
      path: 'app',
      name: 'apptManagement',
      meta: {
        title: '应用管理',
        ignoreKeepAlive: true,
      },
      component: () => import('/@/views/my/platform/app/index.vue'),
    },
  ],
};

export default system;
