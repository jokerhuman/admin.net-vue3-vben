﻿using System;
using Furion.Extras.Admin.NET;

namespace aaa
{
    /// <summary>
    /// aa输出参数
    /// </summary>
    public class CarDto
    {
        /// <summary>
        /// 
        /// </summary>
        public string CarName { get; set; }
        
        /// <summary>
        /// 
        /// </summary>
        public string CarNo { get; set; }
        
        /// <summary>
        /// Id主键
        /// </summary>
        public long Id { get; set; }
        
    }
}
