import { machineapi } from '../../system-api';
export const database = (await machineapi.sysMachineBaseGet()).data.data;
export const infoDatause = (await machineapi.sysMachineUseGet()).data.data;