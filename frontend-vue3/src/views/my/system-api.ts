import {
  UserApi,
  OrgApi,
  DicttypeApi,
  RoleApi,
  PosApi,
  MenuApi,
  AppApi,
  ExlogApi,
  OplogApi,
  VislogApi,
  ConfigApi,
  DictdataApi,
  EnumdataApi,
  TimerApi,
  MachineApi,
  OnlineuserApi,
  CodegenApi,
  CodegenconfigApi,
} from '/@/api_base/api';
import { AuthApi } from '/@/api_base';
export const authApi = new AuthApi(undefined, undefined, defHttp.getAxios());
import { defHttp } from '/@/utils/http/axios';
export const userApi = new UserApi(undefined, undefined, defHttp.getAxios());
export const getuserlist = async (params?: any) => {
  const userInfo = await userApi.sysUserPageGet(
    undefined,
    undefined,
    undefined,
    undefined,
    undefined,
    undefined,
    undefined,
    undefined,
    undefined,
    undefined,
    undefined,
    undefined,
    undefined,
    params.OrgId,
    undefined,
    undefined,
    undefined,
    params.searchstatus,
    undefined,
    undefined,
    undefined,
    params.searchvalue,
    params.page,
    params.pageSize
  );
  // console.log(userInfo);
  return userInfo.data.data;
};
let orgtree;
export const orgApi = new OrgApi(undefined, undefined, defHttp.getAxios());
export const getorgListByPage = async (params?: any) => {
  const userInfo = await orgApi.sysOrgPageGet(
    1,
    undefined,
    undefined,
    params.searchvalue,
    undefined,
    params.page,
    params.pageSize,
    undefined,
    undefined,
    undefined,
    undefined
  );
  return userInfo.data.data;
};
export const getorgtree = async () => {
  if (orgtree != null) {
    return orgtree;
  }
  const Info = await orgApi.sysOrgTreeGet();
  orgtree = Info.data.data;
  //console.log(Info.data);
  return Info.data.data;
};

export function forofa(intput: any, arr: any) {
  for (const info of arr) {
    if (info.value == intput) {
      return info.title;
    }
  }
}
export async function getorgname(intput: any) {
  let data;
  if (orgtree != null) {
    data = orgtree;
  } else {
    data = (await orgApi.sysOrgTreeGet()).data.data;
  }

  for (const info of data) {
    if (info.value == intput) {
      //console.log(info, intput, info.title);
      return info.title;
    }
    const ret = forofa(intput, info.children);

    if (ret != '') {
      return ret;
    }
  }
}
export const dicttypeApi = new DicttypeApi(undefined, undefined, defHttp.getAxios());
export const getdictlist = async () => {
  const Info = await dicttypeApi.sysDictTypeListGet();
  //console.log(Info.data);
  return Info.data.data;
};
export const getdicttypelist = async () => {
  const Info = await dicttypeApi.sysDictTypeDropDownGet('common_status');
  //console.log(Info.data);
  return Info.data.data;
};
let consts_type;
export const getdicttypeconststype = async () => {
  if (consts_type != null) {
    return consts_type;
  }
  const Info = await dicttypeApi.sysDictTypeDropDownGet('consts_type');
  consts_type = Info.data.data;
  return Info.data.data;
};
let code_type;
let lastparams;
export const getdictbycode = async (params?: any) => {
  if (code_type != null && params==lastparams) {
    return code_type;
  }
  const Info = await dicttypeApi.sysDictTypeDropDownGet(params);
  lastparams = params;
  code_type = Info.data.data;
  return Info.data.data;
};
export const roleApi = new RoleApi(undefined, undefined, defHttp.getAxios());
export const getrolelist = async () => {
  const Info = await roleApi.sysRoleDropDownGet();
  return Info.data.data;
};
export const getRoleListByPage = async (params?: any) => {
  const userInfo = await roleApi.sysRolePageGet(
    1,
    params.searchvalue,
    undefined,
    undefined,
    params.page,
    params.pageSize,
    undefined,
    undefined,
    undefined,
    undefined
    //params.searchvalue
  );
  //console.log(userInfo);
  return userInfo.data.data;
};

let poslist;
export const posApi = new PosApi(undefined, undefined, defHttp.getAxios());
export const getposListByPage = async (params?: any) => {
  const userInfo = await posApi.sysPosPageGet(
    params.searchvalue,
    undefined,
    undefined,
    undefined,
    undefined,
    params.page,
    params.pageSize
    //params.searchvalue
  );
  //console.log(userInfo);
  return userInfo.data.data;
};
export const getposlist = async () => {
  if (poslist != null) {
    return poslist;
  }
  const Info = await posApi.sysPosListGet();
  poslist = Info.data.data;
  return Info.data.data;
};

export const menuApi = new MenuApi(undefined, undefined, defHttp.getAxios());
export const getMenuList = async (params?: any) => {
  const userInfo = await menuApi.sysMenuListGet(params.searchvalue, undefined);
  //console.log(userInfo);
  return userInfo.data.data;
};
export const getMenuTree = async () => {
  const userInfo = await menuApi.sysMenuTreeGet();
  //console.log(userInfo);
  return userInfo.data.data;
};

export const appApi = new AppApi(undefined, undefined, defHttp.getAxios());
export const getappList = async () => {
  const userInfo = await appApi.sysAppListGet();
  return userInfo.data.data;
};
export const getappByPage = async (params?: any) => {
  const userInfo = await appApi.sysAppPageGet(
    params.searchvalue,
    undefined,
    undefined,
    params.page,
    params.pageSize
    //params.searchvalue
  );
  //console.log(userInfo);
  return userInfo.data.data;
};

export const exlog = new ExlogApi(undefined, undefined, defHttp.getAxios());
export const getexlogbypage = async (params?: any) => {
  let userInfo;
  if (params.exceptionTime != undefined) {
    userInfo = await exlog.sysExLogPageGet(
      params.name,
      params.className,
      params.methodName,
      params.exceptionMsg,
      undefined,
      params.page,
      params.pageSize,
      params.exceptionTime[0],
      params.exceptionTime[1]
    );
  } else {
    userInfo = await exlog.sysExLogPageGet(
      params.name,
      params.className,
      params.methodName,
      params.exceptionMsg,
      undefined,
      params.page,
      params.pageSize
    );
  }

  //console.log(userInfo);
  return userInfo.data.data;
};
export const oplog = new OplogApi(undefined, undefined, defHttp.getAxios());
export const getoplogbypage = async (params?: any) => {
  let userInfo;
  if (params.opTime != undefined) {
    userInfo = await oplog.sysOpLogPageGet(
      params.name,
      params.success,
      undefined,
      params.location,
      params.page,
      params.pageSize,
      params.opTime[0],
      params.opTime[1]
    );
  } else {
    userInfo = await oplog.sysOpLogPageGet(
      params.name,
      params.success,
      undefined,
      params.location,
      params.page,
      params.pageSize
    );
  }

  //console.log(userInfo);
  return userInfo.data.data;
};

export const vislog = new VislogApi(undefined, undefined, defHttp.getAxios());
export const getvislogbypage = async (params?: any) => {
  let userInfo;
  if (params.visTime != undefined) {
    userInfo = await vislog.sysVisLogPageGet(
      params.name,
      params.success,
      params.visType,
      params.searchValue,
      params.page,
      params.pageSize,
      params.visTime[0],
      params.visTime[1]
    );
  } else {
    userInfo = await vislog.sysVisLogPageGet(
      params.name,
      params.success,
      params.visType,
      params.searchValue,
      params.page,
      params.pageSize
    );
  }
  //console.log(userInfo);
  return userInfo.data.data;
};

export const configapi = new ConfigApi(undefined, undefined, defHttp.getAxios());
export const getconfigByPage = async (params?: any) => {
  const userInfo = await configapi.sysConfigPageGet(
    params.name,
    params.code,
    params.groupcode,
    undefined,
    params.page,
    params.pageSize
    //params.searchvalue
  );
  //console.log(userInfo);
  return userInfo.data.data;
};


export const getdicttypeByPage = async (params?: any) => {
  const userInfo = await dicttypeApi.sysDictTypePageGet(
    params.name,
    params.code,
    undefined,
    params.page,
    params.pageSize
    //params.searchvalue
  );
  //console.log(userInfo);
  return userInfo.data.data;
};

export const dictdataapi = new DictdataApi(undefined, undefined, defHttp.getAxios());
export const getdictdataByPage = async (params?: any) => {
  const userInfo = await dictdataapi.sysDictDataPageGet(
    params.typeId,
    params.value,
    params.code,
    undefined,
    params.page,
    params.pageSize
    //params.searchvalue
  );
  //console.log(userInfo);
  return userInfo.data.data;
};


export const enumdataapi = new EnumdataApi(undefined, undefined, defHttp.getAxios());
export const getTypeEnum = async (params?: any) => {
  const userInfo = await enumdataapi.sysEnumDataListGet(params);
  //console.log(userInfo);
  return userInfo.data.data;
};
export const getTypeEnumbyfid = async (params?: any) => {
  const userInfo = await enumdataapi.sysEnumDataListByFiledGet(params.ename, params.fname);
  //console.log(userInfo);
  return userInfo.data.data;
};

export const timerapi = new TimerApi(undefined, undefined, defHttp.getAxios());
export const gettimerByPage = async (params?: any) => {
  const userInfo = await timerapi.sysTimersPageGet(
    params.jobName,
    undefined,
    undefined,
    undefined,
    undefined,
    undefined,
    undefined,
    undefined,
    undefined,
    undefined,
    undefined,
    undefined,
    undefined,
    undefined,
    undefined,
    undefined,
    undefined,
    params.page,
    params.pageSize
    //params.searchvalue
  );
  //console.log(userInfo);
  return userInfo.data.data;
};

export const machineapi = new MachineApi(undefined, undefined, defHttp.getAxios());
export const onlineuserApi = new OnlineuserApi(undefined, undefined, defHttp.getAxios());
export const getonlineByPage = async (params?: any) => {
  const userInfo = await onlineuserApi.sysOnlineUserPageGet(
    params.searchValue,
    params.page,
    params.pageSize
  );
  //console.log(userInfo);
  return userInfo.data.data;
};

export const codegenapi = new CodegenApi(undefined, undefined, defHttp.getAxios());
export const getcodegenByPage = async (params?: any) => {
  const userInfo = await codegenapi.codeGeneratePageGet(
    params.tableName,
    undefined,
    params.page,
    params.pageSize
  );
  //console.log(userInfo);
  return userInfo.data.data;
};
export const getcodegentableByPage = async () => {
  const userInfo = await codegenapi.codeGenerateInformationListGet();
  //console.log(userInfo);
  return userInfo.data.data;
};
export const codegenconapi = new CodegenconfigApi(undefined, undefined, defHttp.getAxios());
export const getcodegenByconfig = async (params?: any) => {
  const userInfo = await codegenconapi.sysCodeGenerateConfigListGet(undefined,params);
  //console.log(userInfo);
  return userInfo.data.data;
};