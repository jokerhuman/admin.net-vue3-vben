import { BasicColumn } from '/@/components/Table';
import { FormSchema } from '/@/components/Table';
import { h } from 'vue';
import { Switch } from 'ant-design-vue';
import { setRoleStatus } from '/@/api/demo/system';
import { useMessage } from '/@/hooks/web/useMessage';

export const columns: BasicColumn[] = [
  {
    title: '类名',
    dataIndex: 'className',
    width: 200,
  },
  {
    title: '方法名',
    dataIndex: 'methodName',
    width: 100,
  },
  {
    title: '异常名称',
    dataIndex: 'exceptionName',
    width: 100,
  },
  {
    title: '异常信息',
    dataIndex: 'exceptionMsg',
    width: 100,
  },
  {
    title: '异常时间',
    dataIndex: 'exceptionTime',
    width: 180,
  },
  {
    title: '操作人',
    dataIndex: 'name',
    width: 100,
  },
];

export const searchFormSchema: FormSchema[] = [
  {
    field: 'name',
    label: '操作人',
    component: 'Input',
    colProps: { span: 8 },
  },
  {
    field: 'className',
    label: '类名',
    component: 'Input',
    colProps: { span: 8 },
  },
  {
    field: 'methodName',
    label: '方法名',
    component: 'Input',
    colProps: { span: 8 },
  },
  {
    field: 'exceptionMsg',
    label: '异常信息',
    component: 'Input',
    colProps: { span: 8 },
  },
  {
    field: 'exceptionTime',
    label: '操作时间',
    component: 'RangePicker',
    colProps: { span: 8 },
  },
];

export const formSchema: FormSchema[] = [
  {
    field: 'className',
    label: '类名',
    component: 'Input',
  },
  {
    field: 'methodName',
    label: '方法名',
    component: 'Input',
  },
  {
    field: 'exceptionName',
    label: '异常名称',
    component: 'Input',
  },
  {
    field: 'exceptionMsg',
    label: '异常信息',
    component: 'InputTextArea',
  },
  {
    field: 'exceptionTime',
    label: '异常时间',
    component: 'Input',
  },
  {
    field: 'name',
    label: '操作人',
    component: 'Input',
  },
];
