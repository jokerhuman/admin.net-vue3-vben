import { BasicColumn } from '/@/components/Table';
import { FormSchema } from '/@/components/Table';
import { h } from 'vue';
import { Tag } from 'ant-design-vue';
import { Icon } from '/@/components/Icon';
import { getappList, getMenuTree } from '../system-api';

export const columns: BasicColumn[] = [
  {
    title: '菜单名称',
    dataIndex: 'name',
    width: 200,
    align: 'left',
  },
  {
    title: '图标',
    dataIndex: 'icon',
    width: 50,
    customRender: ({ record }) => {
      return h(Icon, { icon: record.icon });
    },
  },
  {
    title: '路由地址',
    dataIndex: 'router',
    width: 180,
  },
  {
    title: '组件',
    dataIndex: 'component',
  },
  {
    title: '排序',
    dataIndex: 'sort',
    width: 50,
  },
  {
    title: '状态',
    dataIndex: 'visible',
    width: 80,
    customRender: ({ record }) => {
      const status = record.visible;
      const enable = status === 'Y';
      const color = enable ? 'green' : 'red';
      const text = enable ? '显示' : '隐藏';
      return h(Tag, { color: color }, () => text);
    },
  },
  {
    title: '所属应用',
    dataIndex: 'application',
    width: 180,
  },
];

const isDir = (type: string) => type === 0;
const isMenu = (type: string) => type === 1;
const isButton = (type: string) => type === 2;

const isno = (openType: string) => openType === 0;
const iscom = (openType: string) => openType === 1;
const islink = (openType: string) => openType === 2;
const iswlink = (openType: string) => openType === 3;
export const searchFormSchema: FormSchema[] = [
  {
    field: 'searchvalue',
    label: '菜单名称',
    component: 'Input',
    colProps: { span: 8 },
  },
  {
    field: 'status',
    label: '状态',
    component: 'Select',
    componentProps: {
      options: [
        { label: '启用', value: '0' },
        { label: '停用', value: '1' },
      ],
    },
    colProps: { span: 8 },
  },
];

export const formSchema: FormSchema[] = [
  {
    field: 'id',
    label: 'Id',
    required: true,
    component: 'Input',
  },
  {
    field: 'type',
    label: '菜单类型',
    component: 'RadioButtonGroup',
    defaultValue: 0,
    componentProps: {
      options: [
        { label: '目录', value: 0 },
        { label: '菜单', value: 1 },
        { label: '按钮', value: 2 },
      ],
    },
    colProps: { lg: 24, md: 24 },
  },
  {
    field: 'name',
    label: '菜单名称',
    component: 'Input',
    required: true,
  },
  {
    field: 'code',
    label: '菜单编号',
    component: 'Input',
    required: true,
  },
  {
    field: 'application',
    label: '所属应用',
    component: 'ApiSelect',
    //defaultValue: [142307070910548, 142307070910547],
    componentProps: {
      api: getappList,
      labelField: 'name',
      valueField: 'code',
    },
    required: false,
  },

  {
    field: 'pid',
    label: '上级菜单',
    component: 'ApiTreeSelect',
    componentProps: {
      api: getMenuTree,
      replaceFields: {
        title: 'title',
        key: 'id',
        value: 'value',
      },
      getPopupContainer: () => document.body,
    },
  },

  {
    field: 'sort',
    label: '排序',
    component: 'InputNumber',
    required: true,
  },
  {
    field: 'icon',
    label: '图标',
    component: 'IconPicker',
    required: false,
    ifShow: ({ values }) => !isButton(values.type),
  },

  {
    field: 'router',
    label: '路由地址',
    component: 'Input',
    required: true,
    ifShow: ({ values }) => !isButton(values.type),
  },
  {
    field: 'permission',
    label: '权限标识',
    component: 'Input',

  },
  {
    ifShow: false,
    field: 'status',
    label: '状态',
    component: 'RadioButtonGroup',
    defaultValue: '0',
    componentProps: {
      options: [
        { label: '启用', value: '0' },
        { label: '禁用', value: '1' },
      ],
    },
  },
  {
    field: 'openType',
    label: '打开方式',
    component: 'RadioButtonGroup',
    defaultValue: 0,
    componentProps: {
      options: [
        { label: '无', value: 0 },
        { label: '组件', value: 1 },
        { label: '内链', value: 2 },
        { label: '外链', value: 3 },
      ],
    },
    ifShow: ({ values }) => !isButton(values.type),
  },
  {
    field: 'link',
    label: '内外链地址',
    component: 'Input',
    ifShow: ({ values }) => islink(values.openType) || iswlink(values.openType),
  },
  {
    field: 'component',
    label: '组件路径',
    component: 'Input',
    ifShow: ({ values }) => !isButton(values.type) && !iswlink(values.openType),
  },
  {
    field: 'weight',
    label: '权重',
    component: 'RadioButtonGroup',
    defaultValue: 1,
    componentProps: {
      options: [
        { label: '系统权重', value: 1 },
        { label: '业务权重', value: 2 },
      ],
    },
    required: true,
  },
  {
    field: 'keepalive',
    label: '是否缓存',
    component: 'RadioButtonGroup',
    defaultValue: '0',
    componentProps: {
      options: [
        { label: '否', value: '0' },
        { label: '是', value: '1' },
      ],
    },
    ifShow: false,
  },

  {
    field: 'visible',
    label: '是否显示',
    component: 'RadioButtonGroup',
    defaultValue: 'Y',
    componentProps: {
      options: [
        { label: '是', value: 'Y' },
        { label: '否', value: 'N' },
      ],
    },
    ifShow: ({ values }) => !isButton(values.type),
  },
];
