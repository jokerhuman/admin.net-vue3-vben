<div align="center">
    <p align="center">
        <img src="https://gitee.com/zhengguojing/admin-net-sqlsugar/raw/master/frontend/public/logo.png" height="50" alt="logo"/>
    </p>
</div>
<div align="center"><h1 align="center">基于vue3的vben修改的Admin.Net</h1></div>
<div align="center"><h2 align="center">Sumuze.Vben</h2></div>
<div align="center"><h3 align="center">前后端分离架构，开箱即用，紧随前沿技术</h3></div>

<div align="center">

</div>

### 🍟 概述

* 基于.NET 5实现的通用权限管理平台（RBAC模式）。整合最新技术高效快速开发，前后端分离模式，开箱即用。
* 前端基于Vue vben admin框架。
* 后台基于Furion框架，Admin.NET。
* 模块化架构设计，层次清晰，业务层推荐写到单独模块，框架升级不影响业务!
* 核心模块包括：用户、角色、职位、组织机构、菜单、字典、日志、多应用管理、文件管理、定时任务等功能。
* 代码量少、通俗易懂、功能强大、易扩展，轻松开发从现在开始！
```
如果对您有帮助，您可以点右上角 “Star” 收藏一下 ，获取第一时间更新，谢谢！
```

### 😎 原始版本 帮助文档

【Admin.NET】

- 👉 [https://gitee.com/zuohuaijun/Admin.NET](https://gitee.com/zuohuaijun/Admin.NET)

【Furion】

- 👉 Furion：  [https://dotnetchina.gitee.io/furion](https://dotnetchina.gitee.io/furion)

【Vue vben admin】

- 👉 [https://gitee.com/annsion/vue-vben-admin](https://gitee.com/annsion/vue-vben-admin)

### ⚡ 更新日志

- 目前只对接用户角色的功能

### 🍄 快速启动

需要安装：VS2019（最新版）、yarn（最新版）

* 启动后台：具体参考：Admin.NET
* 启动前端：打开frontend-vue3文件夹，进行依赖下载，运行yarn命令，再运行 yarn run dev
* 浏览器访问：`http://localhost:3100` （默认前端端口为：3100，后台端口为：5566）
<table>
    <tr>

    </tr>
</table>



### 🍎 效果图

测试站点：http://172.81.207.75:3100/  用户名：superAdmin  密码：123456

<table>
    <tr>
        <td><img src="https://images.gitee.com/uploads/images/2021/0801/032149_a63594ad_675773.png"/></td>
    </tr>
</table>

### 🍖 详细功能




### 👀 数据库切换

1.现在我使用的是mysql，数据库文件已上传
2.如果需要切换数据库，请参考【Admin.NET】


### 🥦 补充说明
* 只改了前端部分，用的vben
* 有问题讨论的小伙伴可加群一起学习讨论。 QQ群【87333204】
<a target="_blank" href="https://qm.qq.com/cgi-bin/qm/qr?k=pN8R-P3pJaW9ILoOXwpRGN2wdCHWtUTE&jump_from=webapi"><img border="0" src="//pub.idqqimg.com/wpa/images/group.png" alt="Admin.NET" title="Admin.NET"></a>

### 💐 特别鸣谢
- 👉 Furion：  [https://dotnetchina.gitee.io/furion](https://dotnetchina.gitee.io/furion)
- 👉 Admin.NET：  [https://gitee.com/zuohuaijun/Admin.NET](https://gitee.com/zuohuaijun/Admin.NET)
- 👉 vben：[https://gitee.com/annsion/vue-vben-admin](https://gitee.com/annsion/vue-vben-admin)


如果对您有帮助，您可以点 "Star" 支持一下，这样才有持续下去的动力，谢谢！！！