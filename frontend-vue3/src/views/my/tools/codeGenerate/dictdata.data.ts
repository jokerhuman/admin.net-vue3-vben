//import { getAllRoleList, isAccountExist } from '/@/api/demo/system';
import { BasicColumn } from '/@/components/Table';
import { FormSchema } from '/@/components/Table';
import { getdictbycode, getdictlist, getorgtree, getposlist } from '../../system-api';
import { Input, Tag } from 'ant-design-vue';
import { defineComponent, h } from 'vue';
//import { values } from 'lodash-es';
export const columns: BasicColumn[] = [
  {
    title: '字段',
    dataIndex: 'columnName',
    width: 100,
  },
  {
    title: '描述',
    dataIndex: 'columnComment',
    edit: true,
    editComponent: 'Input',
    width: 100,
  },
  {
    title: '类型',
    dataIndex: 'netType',
    width: 100,
  },
  {
    title: '作用类型',
    dataIndex: 'effectType',
    edit: true,
    editComponent: 'ApiSelect',
    editComponentProps: {
      api: getdictbycode,
      params: 'code_gen_effect_type',
      labelField: 'value',
      valueField: 'code',
    },
    width: 200,
  },
  {
    title: '字典',
    dataIndex: 'dictTypeCode',
    edit: true,
    editComponent: 'ApiSelect',
    editComponentProps: {
      api: getdictlist,
      labelField: 'name',
      valueField: 'code',
    },
    width: 120,
  },
  {
    title: '列表显示',
    dataIndex: 'whetherTable',
    edit: true,
    editComponent: 'Checkbox',
    editValueMap: (value) => {
      return value.whetherTable ? 'Y' : 'N';
    },
    width: 100,
  },
  {
    title: '增改',
    dataIndex: 'whetherAddUpdate',
    edit: true,
    editComponent: 'Checkbox',
    editValueMap: (value) => {
      return value.whetherAddUpdate ? 'Y' : 'N';
    },
    width: 100,
  },
  {
    title: '必填',
    dataIndex: 'whetherRequired',
    edit: true,
    editComponent: 'Checkbox',
    editValueMap: (value) => {
      return value.whetherRequired ? 'Y' : 'N';
    },
    width: 100,
  },
  {
    title: '是否是查询',
    dataIndex: 'queryWhether',
    edit: true,
    editComponent: 'Checkbox',
    editValueMap: (value) => {
      return value.queryWhether ? 'Y' : 'N';
    },
    width: 100,
  },
  {
    title: '查询方式',
    dataIndex: 'queryType',
    edit: true,
    editComponent: 'ApiSelect',
    editComponentProps: {
      api: getdictbycode,
      params: 'code_gen_query_type',
      labelField: 'value',
      valueField: 'code',
    },
  },
];
