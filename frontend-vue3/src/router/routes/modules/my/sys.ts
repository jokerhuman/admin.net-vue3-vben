import type { AppRouteModule } from '/@/router/types';

import { LAYOUT } from '/@/router/constant';
import { t } from '/@/hooks/web/useI18n';

const sys: AppRouteModule = {
  path: '/sys',
  name: 'Sys',
  component: LAYOUT,
  redirect: '/system/org',
  meta: {
    orderNo: 1900,
    icon: 'ion:settings-outline',
    title: '组织架构',
  },
  children: [
    {
      path: 'org',
      name: 'OrgManagement',
      meta: {
        title: t('routes.demo.system.dept'),
        ignoreKeepAlive: true,
      },
      component: () => import('/@/views/my/sys/org/index.vue'),
    },
    {
      path: 'pos',
      name: 'PosManagement',
      meta: {
        title: t('routes.demo.system.pos'),
        ignoreKeepAlive: true,
      },
      component: () => import('/@/views/my/sys/pos/index.vue'),
    },
  ],
};

export default sys;
