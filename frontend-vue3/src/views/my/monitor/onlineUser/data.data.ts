//import { getAllRoleList, isAccountExist } from '/@/api/demo/system';
import { BasicColumn } from '/@/components/Table';
import { FormSchema } from '/@/components/Table';
import { getdicttypeconststype, getrolelist, getorgtree, getposlist } from '../../system-api';
import { Input } from 'ant-design-vue';
import { defineComponent, h } from 'vue';
//import { values } from 'lodash-es';
export const columns: BasicColumn[] = [
  {
    title: '用户ID',
    dataIndex: 'userid',
  },
  {
    title: '账号',
    dataIndex: 'account',
  },
  {
    title: '昵称',
    dataIndex: 'name',
  },
  {
    title: '登录ip',
    dataIndex: 'lastTime',
  },
  {
    title: '登陆时间',
    dataIndex: 'lastTime',
  },
];

export const searchFormSchema: FormSchema[] = [
  {
    field: 'searchValue',
    label: '关键字',
    component: 'Input',
    colProps: { span: 8 },
  },
];
