import { BasicColumn } from '/@/components/Table';
import { FormSchema } from '/@/components/Table';
import { h } from 'vue';
import { Switch, Tag } from 'ant-design-vue';
import { setRoleStatus } from '/@/api/demo/system';
import { useMessage } from '/@/hooks/web/useMessage';

export const columns: BasicColumn[] = [
  {
    title: '应用名称',
    dataIndex: 'name',
    width: 200,
  },
  {
    title: '应用代码',
    dataIndex: 'code',
    width: 180,
  },
  {
    title: '默认',
    dataIndex: 'active',
    width: 80,
    customRender: ({ record }) => {
      const status = record.active;
      const enable = status === 'Y';
      const color = enable ? 'green' : 'red';
      const text = enable ? '是' : '否';
      return h(Tag, { color: color }, () => text);
    },
  },
  {
    title: '排序',
    dataIndex: 'sort',
    width: 50,
  },
  {
    title: '备注',
    dataIndex: 'remark',
  },
];

export const searchFormSchema: FormSchema[] = [
  {
    field: 'searchvalue',
    label: '搜索关键字',
    component: 'Input',
    colProps: { span: 8 },
  },
  {
    field: 'status',
    label: '状态',
    component: 'Select',
    componentProps: {
      options: [
        { label: '启用', value: 0 },
        { label: '停用', value: 1 },
        { label: '删除', value: 2 },
      ],
    },
    colProps: { span: 8 },
  },
];

export const formSchema: FormSchema[] = [
  {
    field: 'id',
    label: 'Id',
    required: true,
    component: 'Input',
    dynamicDisabled: true,
  },
  {
    field: 'name',
    label: '职位名称',
    required: true,
    component: 'Input',
  },
  {
    field: 'code',
    label: '职位代码',
    required: true,
    component: 'Input',
  },
  {
    field: 'active',
    label: '是否默认',
    component: 'RadioButtonGroup',
    defaultValue: 'N',
    componentProps: {
      options: [
        { label: '是', value: 'Y' },
        { label: '否', value: 'N' },
      ],
    },
  },
  {
    field: 'sort',
    label: '排序',
    required: true,
    component: 'Input',
  },
  {
    label: '备注',
    field: 'remark',
    component: 'InputTextArea',
  },
];
