﻿using Furion.Extras.Admin.NET;
using System;
using System.ComponentModel.DataAnnotations;

namespace aaa
{
    /// <summary>
    /// aa输入参数
    /// </summary>
    public class CarInput : PageInputBase
    {
        /// <summary>
        /// 
        /// </summary>
        public virtual string CarName { get; set; }
        
        /// <summary>
        /// 
        /// </summary>
        public virtual string CarNo { get; set; }
        
    }

    public class AddCarInput : CarInput
    {
    }

    public class DeleteCarInput
    {
        /// <summary>
        /// Id主键
        /// </summary>
        [Required(ErrorMessage = "Id主键不能为空")]
        public long Id { get; set; }
        
    }

    public class UpdateCarInput : CarInput
    {
        /// <summary>
        /// Id主键
        /// </summary>
        [Required(ErrorMessage = "Id主键不能为空")]
        public long Id { get; set; }
        
    }

    public class QueryeCarInput : DeleteCarInput
    {

    }
}
