﻿using Furion.Extras.Admin.NET;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace aaa
{
    public interface ICarService
    {
        Task Add(AddCarInput input);
        Task Delete(DeleteCarInput input);
        Task<Car> Get([FromQuery] QueryeCarInput input);
        Task<dynamic> List([FromQuery] CarInput input);
        Task<dynamic> Page([FromQuery] CarInput input);
        Task Update(UpdateCarInput input);
    }
}